---
layout: page
title: "OSC Catalytic Grant Program - Closed"
---

_Applications for the Spring 2018 round of Catalytic Grants are now closed. Check back in the Fall!_

<!-- <a href="grant-application-form.html" class="button special">Submit a Proposal</a> -->

## About the program

The DIAL Open Source Center's Catalytic Grant program is an offering of financial support for free & open source software projects working in the humanitarian response & international development sectors. These grants are intended to support types of work that have traditionally been neglected or unfinished by these projects for various reasons.

**In general, we fund projects, features or efforts that are well-aligned with the Principles for Digital Development and the mission statement of the OSC.** In order to extend our mission, accepted projects should have a clearly established connection to serving people and communities in low & middle-income countries, or should demonstrate establishing that connection through the work done via grant funds.

We currently plan to run this program twice a year, in May and November.

Please read the information on this page carefully. If the grant sounds appropriate for your project, we strongly encourage you to apply!

### Key dates for this round

- **30 April 2018:** Program announced, proposals accepted
- **15 June 2018:** Deadline for applicants to submit proposals
- **End of June to Early July 2018:** Notification of grant recipients

### Example projects we would prioritize:

- Projects with wider scope (users, purpose, contributor/maintainer organizations) will be favored over projects with narrower scope.
- Efforts on to improve existing projects.
- Proposals to start new multi-stakeholder collaboration for new features or strategies of existing projects.
- Proposals to re-invigorating dormant projects of high potential value.

### Example projects we can't fund:

- New open source projects that don't yet exist, or are otherwise still in a conceptual phase without any existing published code.
- Individuals working independently on non-collaborative projects.
- Projects without multiple stakeholders. (i.e., Multiple contributors, users, etc.)
- Proprietary software projects, e.g., software not licensed under an OSI or FSF approved license.

## Past Catalytic Grant Recipients

For guidance about the scope of your proposal, it may be helpful to learn more about past grant recipients.

### Humanitarian OpenStreetMap Team: Improving Core Tools

This grant to Humanitarian OpenStreetMap Team (HOT) provided resources for a technical community manager, who will create clearly defined pathways for volunteers to contribute code, content, and documentation to four core HOT tools: Tasking Manager, Export Tool, OSM Analytics, and OpenAerialMap. A challenge HOT saw over the past few years ws engaging and on-boarding volunteers to contribute to non-mapping tasks like supporting marketing, writing content, or developing code for HOT’s tools. With the OSC's support, they plan to create the processes, guidelines, and infrastructure to enable volunteers to easily onboard and engage.

### OpenDataKit: Evolving the XLSForm & XForm Specifications

The Open Data Kit (ODK) ecosystem is tied together by two form specifications: the ODK XForms and XLSForm specifications. Enketo LLC previously worked on improving documentation for both. With this grant, Enketo will lead work on the current issue backlog, and define and propose processes for further evolution of the specifications. This work benefits all members of the ODK community at large, by making it easier for developers to build even more tools that will grow the ecosystem, enabling greater interoperability among those tools, and allowing for new features that are more well-considered. Despite being deeply important, this work had unfortunately been overlooked because it was cross-cutting and not explicitly tied to a specific project.

### LibreHealth: Containerization of LibreHealth EHR

The LibreHealth EHR platform is a large application that is used and frequently distributed as a full web stack installation. Installation and setup typically requires a technically savvy user to install all the dependencies such as an operating system, database server, web server, etc. While there have been both prebuilt VM distributions as well as Windows XAMPP style bundles, these are not typically easy to secure, resilient and scalable. This project involves rework on the core structure of the platform so that it can be deployed as a series of containers using common container management standards, such as Kubernetes.

## Current Themes: May 2018

In this round of thematic funding, DIAL is sponsoring up to 6 grants, each up to $25,000 USD, to advance the OSC's mission of fostering healthy, sustainable open source communities and products. We anticipate accepted proposals to be focused on one of the following high-impact areas:

### "Dirty Jobs", part 2

- Improvements to the software development process, such as CI infrastructure, increasing test coverage, or technical backlog grooming
- Improvements to community and contribution, such as improved/consolidated documentation, better packaging, or training materials
- Improvements to the software code quality, such as refactoring code small or updating dependencies to newer versions

### Privacy & responsible data

- Development of features to support user compliance with regulatory requirements such as GDPR
- Features to allow/facilitate anonymization and/or deletion of user-owned content/data
- Improvements to underlying encryption, audit logging, and other security features

### Improving the user experience

- Field-based or laboratory research on user experience challenges faced by product users, admins, etc.
- Development and user testing of UI prototypes to improve known areas of user confusion/frustration
- Review of existing workflow(s) by a UX designer to prioritize improvements

## How to apply

Project leaders should coordinate applications within your open source community, and submit only one single proposal per open source project, representing your community's best idea. Applicants must submit the proposal form on the OSC web site.

Applications are on or before by **May 31, 2018.** Please do not wait until the last minute to apply.

### Proposal review process

1. Your proposal will be reviewed by the OSC Governance Advisory Board's grant review committee, to ensure (a) the basic proposal criteria are included, and (b) your proposal fits the criteria and goals for the program. The reviewers will assess your project to understand its current stage of maturity, practices, and impact, and will write a short narrative evaluation with recommendations to the grant's sponsor.

2. The review committee will forward all reviews and final recommendations for this round to the sponsor funding panel, which in this round consists of DIAL management.

3. Depending on the funds requested, and the complexity of the work to be performed, the review committee and/or the sponsor funding panel may seek independent expert review of the proposal. (For example, if the project is focused on health, feedback might be sought from members of the OSC Health Sustainability Advisory Group, or externally our service delivery partners at Digital Square.) If those external reviews raise additional questions, applicants are given the opportunity to respond in writing to reviewer comments & questions.

4. Recipients will be notified at the end of the review cycle during June 2018.

### Additional information needed from recipients

Selected recipients should be prepared to provide additional information to complete the grant process through our partners at the United Nations Foundation. Specific details will be provided to selected applicants as necessary. This information may include, but may not be limited to:

- DIAL Recipient Assessment form,
- Internal Revenue Service forms, such as [W-9](https://www.irs.gov/pub/irs-pdf/fw9.pdf), [W-8BEN](https://www.irs.gov/pub/irs-pdf/fw8ben.pdf), or [Form 8233](https://www.irs.gov/pub/irs-pdf/f8233.pdf), or similar,
- Basic line-item budget of activities funded by the grant, and
- [DUNS (Data Universal Numbering System) number](https://www.grants.gov/web/grants/applicants/organization-registration/step-1-obtain-duns-number.html), if available.

## Questions about the grant program?

If you have any questions about this grant program, its goals, requirements, or the application process, please [visit the Open Source Center Forum at https://forum.osc.dial.community and post your question](https://forum.osc.dial.community). We will strive to provide initial answers to all questions within 72 hours.

<p>
  <a href="services.html"><span class="icon fa-long-arrow-left"></span> Receive Services</a> |
  <a href="job-opportunity-program-lead.html">Work With Us<span class="icon fa-long-arrow-right">
<p>
