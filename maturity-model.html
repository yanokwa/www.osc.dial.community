---
layout: page
title: "Maturity Model"
---

<h2>Overview</h2>

 <p>The goal of this maturity model is to describe how OSC projects operate, in a concise and high-level way.  It is used as one of the evaluation tools for OSC membership, service, and grant-making decisions. Most importantly, it's used to describe where individual open source projects in the OSC are on the road to becoming more mature and adopting best practices.</p>

 <p>It is meant to be usable outside of the OSC as well, for projects that might want to adopt some or all of these principles. Projects that envision becoming an OSC member might start working towards this to prepare for their move.</p>

 <p>It does not describe all the details of how our projects operate, but aims to capture the best practices of how successful & mature open source projects operate, and point to additional information where needed. To keep the main model as concise as possible we use footnotes for anything that's not part of the core model.</p>

<p>Contrary to other maturity models, we do not define staged partial compliance levels. A mature project complies with all the elements of this model, and other projects are welcome to pick and choose the elements that suit their goals.</p>

 <p>Note that we try to avoid using the word "must" below. The model describes the state of a mature project, as opposed to a set of rules.</p>
<h2>Maturity Model</h2>
 <p>Each item in the model has a unique ID to allow them to be easily referenced elsewhere.</p>

<h3>Software Code</h3>

<ul>
   <li><strong>CD10</strong> The project produces Open Source software, for distribution to the public at no charge. <a href="#foot1" name="cite1"><sup>1</sup></a></li>
   <li><strong>CD20</strong> The project's code is easily discoverable and publicly accessible.</li>
   <li><strong>CD21</strong> The project's code uses mainstream revision control software, such as git.</li>
   <li><strong>CD30</strong> The code can be built in a reproducible way using widely available standard tools.</li>
   <li><strong>CD31</strong> Step-by-step instructions for setting up a developer environment is available for all appropriate platforms/operating systems, with a minimum of manual steps (e.g. running commands in a terminal/shell).</li>
   <li><strong>CD40</strong> The full history of the project's code is available via a source code control system, in a way that allows any released version to be recreated.</li>
   <li><strong>CD50</strong> The provenance of each line of code is established via the source code control system, in a reliable way based on strong authentication of the committer. When third-party contributions are committed, commit messages provide reliable information about the code provenance. <a href="#foot2" name="cite2"><sup>2</sup></a></li>
   <li><strong>CD60</strong> The code contains README, NOTICE, and CONTRIBUTING files (or README sections).</li>
   <li><strong>CD61</strong> A process for claiming bugs is explained in the CONTRIBUTING documentation.</li>
</ul>

<H3>Licenses and Copyright</h3>

<ul>
   <li><strong>LC10</strong> The code is released under one of the preferred copyleft licenses explained in our [Licensing Principles](licensing-principles).</li>
   <li><strong>LC20</strong> Libraries that are mandatory dependencies of the project's code do not create more restrictions than the project's license does. <a href="#foot3" name="cite3"><sup>3</sup></a> <a href="#foot4" name="cite4"><sup>4</sup></a></li>
   <li><strong>LC30</strong> The libraries mentioned in LC20 are available as Open Source software.</li>
   <li><strong>LC40</strong> Committers are bound by an Individual Contributor Agreement (the ["Apache iCLA"](http://www.apache.org/licenses/icla.txt)_ being an example) that defines which code they are allowed to commit and how they need to identify code that is not their own.</li>
   <li><strong>LC50</strong> The copyright ownership of everything that the project produces is clearly defined and documented. <a href="#foot5" name="cite5"><sup>5</sup></a></li>
   <li><strong>LC60</strong> The project name has been checked for trademark issues.</li>
</ul>

<h3>Software Releases</h3>

<ul>
   <li><strong>RE10</strong> Releases consist of source code, distributed using standard and open archive formats that are expected to stay readable in the long term. <a href="#foot6" name="cite6"><sup>6</sup></a></li>
   <li><strong>RE30</strong> Releases are signed and/or distributed along with digests that can be reliably used to validate the downloaded archives.</li>
   <li><strong>RE40</strong> Convenience binaries can be distributed alongside source code but they are not official releases -- they are just a convenience provided with no guarantee.</li>
   <li><strong>RE50</strong> The release process is documented and repeatable to the extent that someone new to the project is able to independently generate the complete set of artifacts required for a release.</li>
   <li><strong>RE60</strong> Release plans are developed and executed in public by the community, and approved by the project's governing body.</li>
   <li><strong>RE70</strong> The project should use the OSC standard release taxonomy, once that is agreed upon.</li>
   <li><strong>RE80</strong> The project has released at least one version.</li>
</ul>

<h3>Software Quality</h3>

<ul>
   <li><strong>QU10</strong> The project is open and honest about the quality of its code. Various levels of quality and maturity for various modules are natural and acceptable as long as they are clearly communicated.</li>
   <li><strong>QU11</strong> In particular, there are either no "monoliths" or "god classes," or they are known and there is a roadmap to refactor them.</li>
   <li><strong>QU12</strong> In particular, there are no custom utility features that could be replaced with common open source tools, including:<ul>
    <li>Databases</li>
    <li>Web application servers/containers</li>
    <li>Object relational mappers</li>
    <li>Dependency/package management</li>
    <li>Authentication</li>
    <li>Continuous integration</li>
    <li>Packaging/installation</li>
   </ul></li>
   <li><strong>QU20</strong> The project puts a very high priority on producing secure software. <a href="#foot7" name="cite7"><sup>7</sup></a></li>
   <li><strong>QU30</strong> The project provides a well-documented channel to report security issues, along with a documented way of responding to them. <a href="#foot8" name="cite8"><sup>8</sup></a></li>
   <li><strong>QU40</strong> The project puts a high priority on backwards compatibility and aims to document any incompatible changes and provide tools and documentation to help users transition to new features.</li>
   <li><strong>QU50</strong> The project strives to respond to documented bug reports in a timely manner.</li>
   <li><strong>QU51</strong> A list of outstanding bugs is open, easily discoverable, prioritized, and responsively triaged.</li>
   <li><strong>QU52</strong> Bugs that are ideal for new contributors are labeled as such.</li>
   <li><strong>QU60</strong> The project must include a unit and integration test suite of sufficient <a href="#foot13" name="cite13"><sup>13</sup></a> coverage, and must document its coverage. Additional performance and scale test capability is desirable.</li>
   <li><strong>QU70</strong> The project must include enough documentation for anyone to test or deploy any of the software.</li>
   <li><strong>QU71</strong> The project must include enough documentation for a software developer of moderate skill to set up their development environment to contribute to the software code.</li>
   <li><strong>QU80</strong> The project must document how it integrates with other OSC Member (or external) projects. Where applicable, the project should be compatible with other active projects.</li>
   <li><strong>QU90</strong> The project has set up a Continuous Integration pipeline for testing and deployment purposes.</li>
   <li><strong>QU100</strong> The project must demonstrate sufficient scalability and document its scalability over various dimensions appropriate to the project.</li>
</ul>

<h3>Community</h3>

<ul>
   <li><strong>CO10</strong> The project has a well-known homepage that points to all the information required to operate according to this maturity model.</li>
   <li><strong>CO20</strong> The community welcomes contributions from anyone who acts in good faith and in a respectful manner and adds value to the project.</li>
   <li><strong>CO30</strong> Contributions include not only source code, but also documentation, constructive bug reports, constructive discussions, marketing and generally anything that adds value to the project.</li>
   <li><strong>CO40</strong> The community is a "holarchy" (see [Governance Principles](governance-principles)) and over time aims to give more rights and responsibilities to contributors who add value to the project.</li>
   <li><strong>CO50</strong> The way in which contributors can be granted more rights such as commit access or decision power is clearly documented and is the same for all contributors.</li>
   <li><strong>CO60</strong> The community operates based on consensus of its members (see CS10) who have decision power. Dictators, benevolent or not, are not welcome in Apache projects.</li>
   <li><strong>CO70</strong> The project strives to answer user questions in a timely manner.</li>
   <li><strong>CO71</strong> The project has a well-known real-time communication platform such as IRC.</li>
   <li><strong>CO72</strong> The project's real-time communications are archived so that users can refer back to past conversations.</li>
   <li><strong>CO73</strong> The community has a forum or other discussion platform for users to ask questions, with reasonable response times from the community.</li>
   <li><strong>CO80</strong> The project has an active and diverse set of contributing members representing various constituencies.</li>
</ul>

<h3>Consensus Building</h3>

<ul>
   <li><strong>CS10</strong> The project maintains a public list of its contributors who have decision power and some sort of documented governance process.</li>
   <li><strong>CS20</strong> Decisions are made by consensus among PMC members <a href="#foot9" name="cite9"><sup>9</sup></a> and are documented on the project's main communications channel. Community opinions are taken into account but the PMC has the final word if needed.</li>
   <li><strong>CS30</strong> Documented voting rules are used to build consensus when discussion is not sufficient. <a href="#foot10" name="cite10"><sup>10</sup></a></li>
   <li><strong>CS40</strong> In Apache projects, vetoes are only valid for code commits and are justified by a technical explanation, as per the Apache voting rules defined in CS30.</li>
   <li><strong>CS50</strong> All "important" discussions happen asynchronously in written form on the project's main communications channel. Offline, face-to-face or private discussions <a href="#foot11" name="cite11"><sup>11</sup></a> that affect the project are also documented on that channel.</li>
</ul>

<h3>Independence</h3>

<ul>
   <li><strong>IN10</strong> The project is independent from any corporate or organizational influence. <a href="#foot12" name="cite12"><sup>12</sup></a></li>
   <li><strong>IN20</strong> Contributors act as themselves as opposed to representatives of a corporation or organization.</li>
   <li><strong>IN30</strong> The project is not highly dependent on any single contributor. There are at least 3 legally independent contributors (e.g., code committers), and there is no single organization that is vital to the success of the project.</li>
</ul>

<h3>Impact</h3>

<ul>
   <li><strong>IM10</strong> The project should be used in real applications and not just in demos. Because not all real-world implementations may be inspected publicly, in such cases statements providing as much details as possible about these implementations should be made.</li>
   <li><strong>IM20</strong> The project should be able to clearly make the case for its importance in the Development and/or Humanitarian sector(s).</li>
</ul>

<h2>About This Document</h2>

<p>The work "DIAL Open Source Center Project Maturity Evaluation" was adapted from ["Apache Project Maturity Model"](https://community.apache.org/apache-way/apache-project-maturity-model.html) by the Apache Software Foundation, licensed under the Apache License version 2.0.</p>

<ul>
<li><a name="foot1"><sup>1</sup></a>  "For distribution to the public at no charge" is straight from the from the ASF Bylaws at http://apache.org/foundation/bylaws.html. <a href="#cite1">&laquo;</a></li>
<li><a name="foot2"><sup>2</sup></a>  See also LC40. <a href="#cite2">&laquo;</a></li>
<li><a name="foot3"><sup>3</sup></a>  It's ok for platforms (like a runtime used to execute our code) to have different licenses as long as they don't impose reciprocal licensing on what we are distributing. <a href="#cite3">&laquo;</a></li>
<li><a name="foot4"><sup>4</sup></a>  http://apache.org/legal/resolved.html has information about acceptable licenses for third-party dependencies <a href="#cite4">&laquo;</a></li>
<li><a name="foot5"><sup>5</sup></a>  In Apache projects, the ASF owns the copyright for the collective work, i.e. the project's releases. Contributors retain copyright on their contributions but grant the ASF a perpetual copyright license for them. <a href="#cite5">&laquo;</a></li>
<li><a name="foot6"><sup>6</sup></a>  See http://www.apache.org/dev/release.html for more info on Apache releases <a href="#cite6">&laquo;</a></li>
<li><a name="foot7"><sup>7</sup></a>  The required level of security depends on the software's intended uses, of course. Expectations should be clearly documented. <a href="#cite7">&laquo;</a></li>
<li><a name="foot8"><sup>8</sup></a>  Apache projects can just point to http://www.apache.org/security/ or use their own security contacts page, which should also point to that. <a href="#cite8">&laquo;</a></li>
<li><a name="foot9"><sup>9</sup></a>  In Apache projects, "consensus" means widespread agreement among people who have decision power. It does not necessarily mean "unanimity". <a href="#cite9">&laquo;</a></li>
<li><a name="foot10"><sup>10</sup></a>  For Apache projects, http://www.apache.org/foundation/voting.html defines the voting rules. <a href="#cite10">&laquo;</a></li>
<li><a name="foot11"><sup>11</sup></a>  OSC projects may have a private mailing list that their governing body is expected to use only when really needed. The private list is typically used for discussions about people, for example to discuss and to vote on PMC candidates privately. <a href="#cite11">&laquo;</a></li>
<li><a name="foot12"><sup>12</sup></a>  Independence can be understood as basing the project's decisions on the open discussions that happen on the project's main communications channel, with no hidden agendas. <a href="#cite12">&laquo;</a></li>
<li><a name="foot13"><sup>13</sup></a>  "Sufficient" can mean different things for different projects, the importance here is that the community can be honest and open about the rationale for choosing what to cover and what not to cover, and about what needs test coverage but doesn't have it. <a href="#cite13">&laquo;</a></li>
</ul>
