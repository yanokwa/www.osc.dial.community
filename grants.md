---
layout: page
title: "OSC Strategic Grant Program - Apply Now!"
---

_Applications for the Spring 2018 round of [Catalytic Grants](catagrant.html) are now closed. Check back in the Fall!_

## Open Source Center Strategic Grants

_Issuance Date: Friday, June 15, 2018_

Dear Applicant:

The Digital Impact Alliance (DIAL), housed within the United Nations Foundation and funded by USAID, SIDA, and Bill and Melinda Gates foundation is seeking grant applications for the Open Source Center’s Strategic Grant Program.

DIAL and the Open Source Center are excited to offer this first round of strategic grants for open source software projects serving the international development and humanitarian sectors. We know how many of you are working on innovative and useful software products, and we hope this round of strategic grants will amplify your efforts.

The anticipated schedule for application and selection is as follows:

* **Issuance Date:** Friday, 15 June 2018
* **Applications Due:** Tuesday,  31 July 2018 at 5:00pm EST
* **Awards Announced:** First week of August (Subject to Change)

Clarifying questions and responses will be posted [here](https://forum.osc.dial.community/t/strategic-grants-q-a-june-2018/335), and will be accepted on a rolling basis through the application deadline.

This schedule is meant to serve as an approximate timeline for grant selection. Dates are subject to change at the discretion of DIAL.

DIAL anticipates awarding up to five grants to eligible organizations whose applications are within the objectives of this grant program, and who demonstrate technical ability and cost reasonableness and realism as outlined throughout the RFA. In line with funding requirements, preference will be given to women-owned organizations and organizations based outside of the United States and Europe.

This RFA in no way obligates DIAL to award grants, nor does it commit them to pay any costs incurred on the preparation and submission of an application. Grants are subject to funder approval, terms and conditions of funding, and availability of funds.

Sincerely,
Jake Watson
_Senior Director, Platforms and Services_

Annexes included with this Request for applications:
* [Annex 1 – Enterprise-Level Quality Improvements](enterprise-quality-improvements.html)
* [Annex 2 – Multi-stakeholder Collaboration](multi-stakeholder-collab.html)
* [Annex 3 – Platform Building and Generalization](platform-building-generalization.html)
* [Annex 4 - Product Consolidation](product-consolidation.html)
* [Annex 5 - Managing Upstream Dependencies and Downstream Forks](managing-upstream-downstream.html)
* [Annex 6 – Applicant Self-Assessment Form](Subgrantee-Organizational-Assessment-Questionnaire.docx)

### SECTION I. PROGRAM DESCRIPTION

#### Objective

DIAL has identified several critical challenges for free & open source software (FOSS) projects serving the sector. These challenges are common issues faced by many FOSS projects and the OSC is committed to helping organizations address these challenges. The overall objective of the Strategic Grant Program is to support solutions to common challenges and to fund projects that foster creativity, scale, and support functionality of the software by removing some of the barriers to FOSS projects meeting full maturity.This grant program seeks to support existing Open Source projects through funding and in-kind support and to find ways that other organizations facing similar challenges might address them.

#### Background and Detailed Program Description

**About DIAL and the Open Source Center**

_Grant Sponsor: Digital Impact Alliance (DIAL) at United Nations Foundation_

DIAL originated to bring the public and private sectors together to realize an inclusive digital society that connects everyone to life-enhancing and life-enabling technology. DIAL is staffed by a global team of technology researchers, developers, investors, negotiators, and policymakers. It is supported by world-class foundations and development agencies and guided by a Board of leading emerging market entrepreneurs, technologists and development experts. With this leadership, DIAL is uniquely positioned to serve as a neutral broker, bringing together government, industry, and other development stakeholders to promote new solutions to old problems.

_Grant Administrator: DIAL Open Source Center Governance Advisory Board_

The mission of the DIAL Open Source Center is to convene a vibrant and inclusive community for builders of free and open source software -- promoting knowledge sharing, collaboration and co-investment in technology and human capacity to support positive social change in communities around the world. The Center’s participating members believe that an ecosystem with mature open source products, supported by individuals representing diverse backgrounds and organizations, is a key to delivering open and sustainable digital products and services that honor the Principles of Digital Development. Such an approach addresses top challenges of both practitioners and engineers.
While these grants aim to promote good practice of all the Principles of Digital Development, the categories in this round of funding are most focused on Understanding the Ecosystem, Building for Sustainability, Reusing and Improving, and Using Open Source.
The Open Source Center grants programs are designed to support existing Open Source Projects to further their reach, functionality, and creativity.   DIAL seeks to provide Strategic Grants to organizations facing common challenges and road blocks. Elligible proposals will involve one or more of the following activity categories:

*	Enterprise-Level Quality Improvements-- Software product development is typically tied to programmatic goals, which can result in products that are unable to achieve enterprise-level security, stability and quality during the lifecycle of the programmatic funding, and who struggle to find resources for long-term sustainability and maintenance of those products. Additionally, as new products gain popularity, older projects need to be able build additional interoperability features to continue to maximize their impact and reach.
*	Multi-stakeholder Collaboration-- Open source products for development are often funded and developed within one organizational host which often lacks the time and resources necessary to foster a more diverse community of contributors and users, leading to fragmentation and siloes in the ecosystems and products that are “open source in name only:” the product’s code is available online and licensed as open-source but remains largely maintained & contributed to by a single organizational host.
*	Platform Building and Generalization-- Software product development is typically tied to programmatic goals, which results in tools that are specialized to a particular sector or use case. These products have limited reach and are therefore more challenging to maintain (due to lower demand) and cannot achieve the broad impact that a more general platform could in multiple sectors.
*	Product Consolidation--Fragmented funding, siloed projects and organizations, and a general lack of awareness of existing products has led to duplicative efforts and an ecosystem of open source projects with overlapping capabilities. These products have limited resources to sustain themselves, both financial and human. This presents an opportunity for disparate communities of product maintainers to combine efforts and product functionality to produce a single product with a broader maintainer base, strongest features, and furthest potential reach.
*	Managing Upstream Dependencies and Downstream Forks--There is a mis-match between the fast pace of programmatic work when deploying digital technologies “in the field” and the slower, more cautious work to update the project to a newer version of an underlying framework, or to incorporate new features into the core of an open source product’s codebase. As a result, customizations that must be written with programmatic deadlines in mind are often never reincorporated as reusable features into the core product, leading to an ecosystem of custom “forks” of open source products all with single deployments, and a less feature-rich core product that has to be re-customized with each new programmatic use, leading to wasted and/or duplicative effort.

Under these Strategic Grants, organizations will be required to provide innovative approaches to addressing these challenges as outlined in the hyperlinked scopes of work. Applicants must also explain how they will make their open source software more accessible and how they will embody the Digital Principles.

Applicants are encouraged to specify their needs for technical assistance and/or training in their application as well.

#### Authority/Governing Regulations

Funding for the Strategic Grants is provided through a combination of funding from USAID, SIDA, and Bill and Melinda Gates Foundation. At the time of selection, grantees will be informed of regulations pertaining to their grant award. All grant awards are subject to restrictions from funding sources.

### SECTION II. AWARD INFORMATION

For as many as 5 grant awards, DIAL anticipates providing up to $900,000 USD total and up to 480 hours total of complementary in-kind technical assistance through participation in the Open Source Center program. This award is expected to span six months of project activity, with an option to extend. Examples of the types of technical assistance services available through the Open Source Center can be found here: http://www.osc.dial.community/services-details.html.
All applications should be in US Dollars, as awards will be issued in US Dollars. The final amount will be dependent upon grant activities and final negotiation and may be lower or higher than that range. The duration of any grant award under this solicitation is expected to be no longer than 12 months. The estimated start date of grants awarded under this solicitation is August 3, 2018.  The specific type of grant will be determined during the negotiation process with each individual organization.

### SECTION III. ELIGIBILITY

**Eligibility Criteria**

*	Proposal extends, integrates, or improves one or more existing open source project(s) that is no longer in a conceptual phase, and has existing published code.
* Target product(s) must be made available under a license approved by the Open Source Initiative or Free Software Foundation.
* Applicants must be able to demonstrate successful past performance in implementation of software product development and documentation.
* Only one application per product(s) is permitted under this RFA.
* Applicants must identify a suitable recipient of funds such as a product ecosystem partner or fiscal sponsor, which displays sound management in the form of financial, administrative, and technical policies and procedures and present a system of internal controls that safeguard assets; protect against fraud, waste, and abuse; and support the achievement of program goals and objectives.

If you are unsure whether your project is eligible or not, please don't hesitate to [ask on the OSC forum](https://forum.osc.dial.community/t/strategic-grants-q-a-june-2018/335).

Upon selection, grantees obtain (if necessary) and provide a Data Universal Numbering System (DUNS) number. The CBCSA will assist successful applicants with this process. DUNS numbers can be obtained online at http://fedgov.dnb.com/webform/pages/CCRSearch.jsp.

### SECTION IV – APPLICATION AND SUBMISSION INFORMATION

#### Instructions to Applicants
A complete application includes all required documentation and any subsequent amendments:

* Proposal including Concept for project in consideration of grant objectives and merit criteria
* Budget and Budget Narrative
* [Completed Risk Assessment Form](Subgrantee-Organizational-Assessment-Questionnaire.docx)
* W-9

Application Due July 31, 2018

Submission Instructions:

Applications should be emailed to grants@dial.community. Applicants must propose strategies for the implementation of the program description described above, introducing innovations that are appropriate to their organizational strengths. Applications should be typewritten in a standard text format (.odf, .txt, .md, .rst, .pdf, .docx, etc.), no longer than 10 pages in standard text size, format and margins.

#### Applicant Self-Assessment

All organizations selected for award are subject to a pre-award risk assessment conducted by DIAL, to ascertain whether the organization has the minimum management capabilities required to handle US government funds.

#### Ineligible Expenses

DIAL grant funds may not be utilized for the following:

* Construction or infrastructure activities of any kind.
* Ceremonies, parties, celebrations, or “representation” expenses.
* Purchases of restricted goods, such as: restricted agricultural commodities, motor vehicles including motorcycles, pharmaceuticals, medical equipment, contraceptive products, used equipment; without the previous approval of DIAL, or prohibited goods, prohibited goods under USAID regulations, including but not limited to the following: abortion equipment and services, luxury goods, etc.
* Alcoholic beverages.
* Purchases of goods or services restricted or prohibited under the prevailing USAID source/ nationality (Burma (Myanmar), Cuba, Iran, North Korea, (North) Sudan and Syria).
* Any purchase or activity, which has already been made.
* Purchases or activities unnecessary to accomplish grant purposes as determined by the DIAL Project.
* Prior obligations of and/or, debts, fines, and penalties imposed on the Grantee.
* Creation of endowments.

### SECTION V. APPLICATION EVALUATION

#### Merit Review Criteria

Merit Review Criteria | Merit Review Category	Rating (Points)
---|---
Feasibility of Design and Technical Approach | 20
Impact on Target Product | 15
Current Impact of Product for End Users | 15
Change in Impact for Ecosystem and End Users | 10
Technical Capacity | 10
Cost Effectiveness | 10
Alignment with Digital Principles | 10
Inclusivity | 10
**Overall Rating (out of 100 points)** | **100**

These merit review criteria elements are described more fully below.

* _Feasibility of Design and Technical Approach._ The quality and feasibility of the application in terms of the viability of the proposed technical approach. The proposed technical approach can reasonably be expected to produce the intended outcomes. Proposed activities are appropriate to achieve desired outcomes. Innovativeness of the proposed approach. Realistic and sufficient work plan for achieving project outcomes. Proposed testing tools and plans will also be appraised. 20 points
* _Impact on Target Product._ Applicants must clearly describe how their proposed activities will benefit the target product in terms of usefulness, quality, and/or functionality. 15 points
* _Current Impact of Product for End Users._ The applicant should describe the number and scale of existing deployments of the target product, and also estimate the number of end users impacted by the proposed work. 15 points
* _Change in Impact of Product for Ecosystem and End Users._ The degree to which proposed activities will directly or indirectly stimulate and build the reach and effectiveness of other development organizations and other product consumers as well as resources to replicate, develop, reuse, or implement activities deploying the product or products in their programs. Additionally, the degree to which proposed activities will expand the reach of existing or new deployments when compared against existing capabilities of the target product. 10 points
* _Technical Capacity._ The applicant must provide CV’s or other biographical information on 1-3 individuals to proposed to perform the work, demonstrating experience and capabilities necessary to achieve the proposed activities. 10 points
* _Cost Effectiveness._ To achieve maximum points in this category, the applicant must demonstrate how the budget reflects best use of both new grant resources and existing  product/project resources. 10 points
* _Alignment with the Digital Principles._ To achieve maximum points in this category, applicants must clearly indicate an understanding of the existing ecosystem as well as address issues of scale, sustainability, privacy, security. Applications should demonstrate how the proposal plans to reuse and improve existing solutions, and how any new technology development is not duplicative of existing solutions. 10 points
* _Inclusivity._ The extent to which the proposed activity includes a gender, youth or LGBT population component or represents a strong commitment to women, youth and the LGBT community as beneficiaries. 10 points

<p>
  <a href="services.html"><span class="icon fa-long-arrow-left"></span> Receive Services</a> |
  <a href="job-opportunity-program-lead.html">Work With Us<span class="icon fa-long-arrow-right">
<p>
