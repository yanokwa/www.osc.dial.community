---
layout: page
title: "Finance Principles"
---

**Historically, many potential member projects in the Open Source Center have grown based on responses to needs expressed by organizations and entities funding international development initiatives.** These funders may have spearheaded or co-facilitated the development of an entire software project, or they may have sponsored the development of new or improved features in software. Often, this funding has been based on time-limited and scope-limited grants.

**We encourage member projects to seek potential funding opportunities** for development and advancement of their specific software products. Through its partner Software Freedom Conservancy, OSC offers a non-profit fiscal home for management of those funds, which can apply such resources as funding or reimbursement for individual contributors and volunteers who perform specific tasks. **Such decisions will be made by the Project Leader (in consultation with their entire project team) along with the OSC Governance Advisory Board.** The OSC Technical Steering Committee exists to assist project teams with prioritization of feature development or that project's technical efforts.

Unfortunately, a business model in which most resources are dedicated **only** toward growth is rarely a sustainable one. A mature FOSS project – especially one of a larger size – requires effort in traditional “engineering management” tasks such as product &amp; project management, maintenance of a feature development backlog, performance engineering, release management, support, and maintenance of bugs as they are discovered. While FOSS development can often be done by volunteers, history has shown that these efforts alone are not enough to maintain these critical needs. Therefore, additional consideration should be paid to sustainability and resourcing the project as a whole.

As such, the community will **additionally seek separate core shared resources** for initiatives such as management, maintenance of projects, events, and infrastructure. **A to-be-determined percentage of all OSC funding will be earmarked for these types of activities,** to prevent investment of all community resources on new features at the neglect of maintaining the high level of software quality our customers expect.
